<?php

namespace BSearch;

use Exception;

class BSearch {

    public static function search($arr, $val) {
        if(!self::_sorted($arr)) {
            throw new \InvalidArgumentException("Array is not sorted!");
        }

        return self::_search($arr, $val, 0, count($arr) - 1);
    }

    private static function _sorted(&$arr) {

        for($i = 0, $j = count($arr) - 1; $i < count($arr) / 2; $i++, $j--) {
            if($arr[$i] > $arr[$j])
                return false;
        }

        return true;
    }

    private static function _search(&$arr, &$val, $lhs, $rhs) {

        if($lhs < $rhs) {
            $mid = $lhs + ($rhs - $lhs) / 2;

            if($arr[$mid] == $val) {
                return $mid;
            } else if($arr[$mid] < $val) {
                return self::_search($arr, $val, $mid + 1, $rhs);
            }
            else {
                return self::_search($arr, $val, $lhs, $mid - 1);
            }
        }

        return -1;
    }
}