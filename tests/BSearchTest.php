<?php

use PHPUnit\Framework\TestCase;

require __DIR__ . "/../src/BSearch.php";
use BSearch\BSearch;

class BSearchTest extends TestCase {
    public function testIndex() {
        $arr = array(1, 2, 3, 4, 5);
        $this->assertEquals(-1, BSearch::search($arr, 13));
    }

    public function testException() {
        $arr = array(5, 1, 4, 2, 3);
        $this->expectException(\InvalidArgumentException::class);
        BSearch::search($arr, 13);
    }
}