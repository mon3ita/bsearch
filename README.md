# BSearch

## Install

`composer require mon3ita/bsearch`

## Usage

```php
<?php

use BSearch\BSearch;

$arr = array(1, 2, 3, 4, 5);

$index = BSearch::search($arr, 3);

print($index); //2
```

